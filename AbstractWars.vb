Option Explicit On
Imports System
Imports System.Collections.Generic
Imports TpC = System.Tuple(Of Integer, Integer, Integer)

Public Class AbstractWars
    Dim curStep As Integer = 0
    Dim troopSpeed As Integer
    Dim baseCount As Integer
    Dim baseX() As Integer
    Dim baseY() As Integer
    Dim baseGrow() As Integer
    Dim basesPrev() As Integer
    Dim distTable(,) As Double
    Dim moveTimeTable(,) As Integer
    Public Function init(baseLocations As Integer(), speed As Integer) As Integer
        troopSpeed = speed
        baseCount = baseLocations.Length \ 2
        ReDim baseX(baseCount - 1), baseY(baseCount - 1)
        For i As Integer = 0 To baseCount - 1
            baseX(i) = baseLocations(i * 2)
            baseY(i) = baseLocations(i * 2 + 1)
        Next i
        ReDim baseGrow(baseCount - 1), basesPrev(baseCount * 2 - 1)
        ReDim distTable(baseCount - 1, baseCount - 1)
        ReDim moveTimeTable(baseCount - 1, baseCount - 1)
        For i As Integer = 0 To baseCount - 2
            For j As Integer = i + 1 To baseCount - 1
                Dim dx As double = baseX(j) - baseX(i)
                Dim dy As double = baseY(j) - baseY(i)
                Dim dist As double = Math.Sqrt(dx * dx + dy * dy)
                Dim moveT As Integer = CInt(Math.Ceiling(dist / CDbl(speed)))
                distTable(i, j) = dist
                distTable(j, i) = dist
                moveTimeTable(i, j) = moveT
                moveTimeTable(j, i) = moveT
            Next j
        Next i
        init = 0
    End Function
    Dim attacks As New List(Of TpC)()
    Public Function sendTroops(bases As Integer(), troops As Integer()) As Integer()
        Dim ret As New List(Of Integer)()
        curStep += 1
        Select Case curStep
        Case 1
            Array.Copy(bases, basesPrev, bases.Length)
        Case 2
            For i As Integer = 0 To baseCount - 1
                baseGrow(i) = bases(i * 2 + 1) - basesPrev(i * 2 + 1)
            Next i
        Case Else
            Dim damages(baseCount * 2 - 1) As Integer
            Dim tmp As New List(Of TpC)()
            For Each e As TpC In attacks
                If e.Item2 <= 1 Then Continue For
                damages(e.Item1 * 2) += e.Item3
                damages(e.Item1 * 2 + 1) = Math.Max(damages(e.Item1 * 2 + 1), e.Item2)
                tmp.Add(New TpC(e.Item1, e.Item2 - 1, e.Item3))
            Next e
            Dim rems As Integer = 0
            For i As Integer = 0 To baseCount - 1
                If bases(i * 2) <> 0 Then Continue For
                rems += 1
                If bases(i * 2 + 1) < 950 Then Continue For
                Dim k As Integer = -1
                For j As Integer = 0 To baseCount - 1
                    If bases(j * 2) = 0 Then Continue For
                    If baseGrow(j) > baseGrow(i) Then Continue For
                    If damages(j * 2) * 10 > (bases(j * 2 + 1) + baseGrow(j) * damages(j * 2 + 1)) * 13 Then Continue For
                    If k < 0 OrElse distTable(i, j) < distTable(i, k) Then
                        k = j
                    End If
                Next j
                If k < 0 Then Continue For
                ret.Add(i)
                ret.Add(k)
                Dim ut As Integer = bases(i * 2 + 1) \ 2
                damages(k * 2) += ut
                damages(k * 2 + 1) = Math.Max(damages(k * 2 + 1), moveTimeTable(i, k))
                tmp.Add(New TpC(k, moveTimeTable(i, k), ut))
                bases(i * 2 + 1) -= ut
            Next i
            If rems < 6 Then
                For i As Integer = 0 To baseCount - 1
                    If bases(i * 2) <> 0 Then Continue For
                    Dim cnt As Integer = 0
                    For j As Integer = 0 to UBound(troops) Step 4
                        If troops(j) = 0 Then Continue For
                        Dim dx As Integer = troops(j + 2) - baseX(i)
                        Dim dy As Integer = troops(j + 3) - baseY(i)
                        Dim dist As Integer = dx * dx + dy * dy
                        If dist <= troopSpeed * troopSpeed + 1 Then
                            cnt += 1
                        End If
                    Next j
                    If cnt = 0 Then Continue For
                    Dim k As Integer = -1
                    For j As Integer = 0 To baseCount - 1
                        If i = j Then Continue For
                        If k < 0 OrElse distTable(i, j) > distTable(i, k) Then
                            k = j
                        End If
                    Next j
                    If k < 0 Then Exit For
                    Do While bases(i * 2 + 1) > 1
                        ret.Add(i)
                        ret.Add(k)
                        bases(i * 2 + 1) -= bases(i * 2 + 1) \ 2
                    Loop
                Next i
            End If
            attacks = tmp
        End Select
        Return ret.GetRange(0, Math.Min(baseCount * 2, ret.Count)).ToArray()
    End Function
End Class