Imports Console = System.Console

Public Module Main

    Public Sub Main()
        Try
            Dim aw As New AbstractWars()
            
            Dim baseLocationsSize As Integer = CInt(Console.ReadLine())
            Dim baseLocations(baseLocationsSize - 1) As Integer
            For i As Integer = 0 To baseLocationsSize - 1
                baseLocations(i) = CInt(Console.ReadLine())
            Next i
            Dim speed As Integer = CInt(Console.ReadLine())
            Dim initRet As Integer = aw.init(baseLocations, speed)
            Console.WriteLine(initRet)
            Console.Out.Flush()
            For t As Integer = 1 To 2000
                Dim basesSize As Integer = CInt(Console.ReadLine())
                Dim bases(basesSize - 1) As Integer
                For i As Integer = 0 To basesSize - 1
                    bases(i) = CInt(Console.ReadLine())
                Next i
                Dim troopsSize As Integer = CInt(Console.ReadLine())
                Dim troops(troopsSize - 1) As Integer
                For i As Integer = 0 To troopsSize - 1
                    troops(i) = CInt(Console.ReadLine())
                Next i
                Dim ret() As Integer = aw.sendTroops(bases, troops)
                Console.WriteLine(ret.Length)
                For Each x In ret
                    Console.WriteLine(x)
                Next x
                Console.Out.Flush()
            Next t
        Catch ex As Exception
        End Try
    End Sub
    
End Module