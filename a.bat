@echo off
if "%~1"=="" goto defaultseed
java -jar tester.jar -exec "AbstractWars.exe" -seed %*
exit /b
:defaultseed
java -jar tester.jar -exec "AbstractWars.exe" -seed 1